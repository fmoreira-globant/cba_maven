package cba.selenium.basics;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class NavigationTestNGAdvanced {
	
	private WebDriver driver = null;
	
	@DataProvider(name = "Credentials")
	public static Object[][] credentials() {
		return new Object[][] { { "test", "test1234" }}; 
	}
	
	@BeforeMethod
	public void setUp() throws Exception {
		//Building the driver
		ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        driver.get("http://54.212.7.174/admin");
	}

	@AfterMethod
	public void tearDown() {
		driver.close();
	}
	
	@Test(dataProvider = "Credentials")
	public void testLogin() throws InterruptedException {
		
        // Wait for the login textbox
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.
				presenceOfElementLocated(By.id("user_login")));
        
		//Login
		WebElement login = driver.findElement(By.id("user_login"));
		WebElement password = driver.findElement(By.id("user_pass"));
		WebElement btn_login = driver.findElement(By.id("wp-submit"));
		
		login.sendKeys("test");
		password.sendKeys("test1234");
		btn_login.click();
		
		wait.until(ExpectedConditions.
				elementToBeClickable(By.id("wp-admin-bar-root-default")));
		WebElement header = driver.findElement(By.id("wp-admin-bar-root-default"));
		
		WebElement headerLink = header.findElement(By.xpath("//a[contains(text(),'Test Automation')]"));
		
		assertTrue(headerLink.isDisplayed(), "The Link is displayed");
		
		//Back to the HomePage
		driver.get("http://54.212.7.174");
		
		//Get all the posts
		wait.until(ExpectedConditions.
				visibilityOfAllElementsLocatedBy(By.xpath("//article")));
		
		List<WebElement> postList = driver.findElements(By.xpath("//article"));
		
		assertFalse(postList.isEmpty(), "The list contains at least one post");
		
		//Print article results
		for(WebElement post : postList)
		{
			System.out.println(post.getText());
		}
	}

}

package cba.selenium.pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminPage extends BasePage{
	public static final String HEADER_CONTAINER ="wp-admin-bar-root-default";
	
	public AdminPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id=HEADER_CONTAINER)
	private WebElement header;

	public boolean isHeaderDisplayed(){
		WebElement headerLink = header.findElement(By.xpath("//a[contains(text(),'Test Automation')]"));
		return headerLink.isDisplayed();
	}
}

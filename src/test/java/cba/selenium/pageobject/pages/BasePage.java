package cba.selenium.pageobject.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
	
	protected WebDriver driver;
	protected WebDriverWait dwait;
	
    public BasePage(WebDriver driver) {
    	this.driver = driver;
    	this.dwait = new WebDriverWait(driver, 10,2);
    	PageFactory.initElements(driver, this);
    }

}

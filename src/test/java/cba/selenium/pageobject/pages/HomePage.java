package cba.selenium.pageobject.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{
	
	public HomePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath="//input[@type='search']")
	private WebElement inputSearch;
	
	@FindBy(xpath="//article")
	private List<WebElement> articles;
	
	public List<String> getPostsTexts(){
		List<String> textList = new ArrayList<String>();
		for(WebElement post : articles)
		{
			textList.add(post.getText());
		}
		return textList;
	}

	public boolean isSearchDisplayed() {
		return inputSearch.isDisplayed();
	}
	
}

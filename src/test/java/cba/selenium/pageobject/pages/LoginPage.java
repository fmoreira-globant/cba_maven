package cba.selenium.pageobject.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage{
	public static final String INPUT_USERNAME ="user_login";
	public static final String INPUT_PASSWORD ="user_pass";
	public static final String BUTTON_LOGIN = "wp-submit";
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id=INPUT_USERNAME)
	private WebElement inputUserName;
	
	@FindBy(id=INPUT_PASSWORD)
	private WebElement inputPassword;

	@FindBy(id=BUTTON_LOGIN)
	private WebElement buttonLogIn;

	public AdminPage login(String username, String password){
		inputUserName.sendKeys(username);
		inputPassword.sendKeys(password);
		buttonLogIn.click();
		return new AdminPage(this.driver);
	}
	
}

package cba.selenium.pageobject.tests;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cba.selenium.pageobject.pages.AdminPage;
import cba.selenium.pageobject.pages.HomePage;
import cba.selenium.pageobject.pages.LoginPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;

public class PageObjectTest {
	
	private WebDriver driver = null;
	
	@DataProvider(name = "Credentials")
	public static Object[][] credentials() {
		return new Object[][] { { "test", "test1234" }}; 
	}
	
	@BeforeMethod
	public void setUp() throws Exception {
		//Building the driver
		ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        driver.get("http://54.212.7.174/admin");
	}

	@AfterMethod
	public void tearDown() {
		driver.close();
	}
	
	//Plain and Basic PO example using TestNG
	@Test(dataProvider = "Credentials")
	public void testLogin(String username, String password) throws InterruptedException {
		
    	LoginPage lp = new LoginPage(driver);
    	AdminPage ap = lp.login(username, password);
    	assertTrue(ap.isHeaderDisplayed(), "The Link is not displayed");
		
		HomePage hp = NavigateToHomePage();
    	assertTrue(hp.isSearchDisplayed());
		
    	List<String> postsTexts = hp.getPostsTexts();	
		assertFalse(postsTexts.isEmpty(), "The list contains at least one post");
	}
	
	public HomePage NavigateToHomePage()
	{
		this.driver.get("http://54.212.7.174");
		return new HomePage(this.driver);
	}

}
